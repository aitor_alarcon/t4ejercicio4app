/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T4Ejercicio4App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Creamos la variable N y la inicializamos
		int N = 50;
		
		// Muestro el valor inicial de N
		System.out.println("Valor inicial de N = " + N);
		
		// Incremento N en 77
		N = N + 77;
		
		// Muestro el valor N incrementado
		System.out.println("N + 77 = " + N);
		
		// Decremento N en 3
		N = N - 3;
		
		// Muestro el valor N decrementado
		System.out.println("N - 3 = " + N);
		
		// Duplicamos el valor N
		N = N * 2;
		
		// Muestro el valor N duplicado
		System.out.println("N * 2 = " + N);
		
		
	}

}
